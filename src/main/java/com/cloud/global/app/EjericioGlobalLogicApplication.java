package com.cloud.global.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjericioGlobalLogicApplication {

	public static void main(String[] args) {
		SpringApplication.run(EjericioGlobalLogicApplication.class, args);
	}

}
